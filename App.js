//import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { AppLoading } from 'expo';
import { NavigationContainer } from '@react-navigation/native';

import AuthNavigator from './app/navigation/AuthNavigator';
import navigationTheme from './app/navigation/navigationTheme';
import AppNavigator from './app/navigation/AppNavigator';
import AsyncStorage from '@react-native-community/async-storage';
import OfflineNotice from './app/components/OfflineNotice';
import AuthContext from './app/auth/context';
import authStorage from './app/auth/storage';

export default function App() {
    const [user,setUser] = useState();
    const [isReady,setIsReady] = useState(false);

    const restoreUser = async () => {
        const user = await authStorage.getUser();
        if(user) setUser(user);
    }

    /* useEffect(() => {
         restoreToken();
     },[]);
    */
    //NetInfo.fetch().then((netInfo) => console.log(netInfo));
    //NetInfo.addEventListener((netInfo) => console.log(netInfo));

    const demo= async() => {

      try {
          await AsyncStorage.setItem("person", JSON.stringify({id : 1}));
          const value = await AsyncStorage.getItem("person");
          console.log(value);
          //const person = JSON.parse(value);
          //console.log("Async Storage: "+person);
      } catch (error) {
          console.log("Error: "+ error);
      }
       
    }
    demo();
    if(!isReady)
        return(
            <AppLoading startAsync={restoreUser} onFinish={ () => setIsReady(true)} />
        );
    return (
        <AuthContext.Provider value={{user,setUser}}>
            <OfflineNotice/>
            <NavigationContainer theme={navigationTheme}>
                {user ? <AppNavigator/> : <AuthNavigator/>}
            </NavigationContainer> 
        </AuthContext.Provider>
    );
}

