import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import * as Yup from 'yup';

import CategoryPickerItem from '../components/CategoryPickerItem';
import { AppForm, AppFormField, AppFormImagePicker, AppFormPicker, SubmitButton } from '../components/forms';
import Screen from '../components/Screen';
import useLocation from '../hooks/useLocation';
import listingsApi from '../api/listings';
import listings from '../api/listings';
import UploadScreen from './UploadScreen';

const validateSchema = Yup.object().shape({
    title: Yup.string().required().label("Title"),
    price: Yup.string().required().min(1).max(10000).label("Price"),
    description: Yup.string().optional().label("Description"),
    category: Yup.string().required().nullable().label("Category"),
    images: Yup.array().min(1,"Please select atleast one image")
});

const categories = [ 
    {label: "Furniture", value: 1, backgroundColor: "#fc5c65", icon: "floor-lamp" },
    {label: "Clothing", value: 2, backgroundColor: "#2bcbba", icon: "shoe-heel" },
    {label: "Cameras", value: 3, backgroundColor: "#fed330", icon: "camera" },
    {label: "Cars", value: 4, backgroundColor: "#fd9644", icon: "car" },
    {label: "Games", value: 5, backgroundColor: "#26de81", icon: "cards" },
    {label: "Sports", value: 6, backgroundColor: "#45aaf2", icon: "basketball" },
    {label: "Movies & Music", value: 7, backgroundColor: "#4b7bec", icon: "headphones" },
    {label: "Books", value: 8, backgroundColor: "yellowgreen", icon: "book-open" },
    {label: "Others", value: 9, backgroundColor: "violet", icon: "outlook" },

];

function ListingEditScreen(props) {
    
    const location = useLocation();
    const [uploadVisible, setUploadVisible] = useState(false);
    const [progress, setProgress] = useState(0);

    const handleSubmit = async (listing, {resetForm}) => {
        setProgress(0);
        setUploadVisible(true);
        //two arguments in addListing function
        const result= await listingsApi.addListing(
            {...listing, location}, 
            (progress) => setProgress(progress)
        );
        
        if(!result.ok){
            setUploadVisible(false);
            return alert('Could not save the listing'); 
        }  
        resetForm();  
    };

    return (
        <Screen style={styles.container}>
            <UploadScreen 
                onDone={() => setUploadVisible(false)} 
                progress={progress} 
                visible={uploadVisible} />
            <AppForm 
                initialValues={{title: "", price: "", description: "", category: null, images: []}}
                onSubmit={handleSubmit}
                validationSchema={validateSchema} >
                    <AppFormImagePicker name="images"/>
                    <AppFormField name="title"
                        placeholder="Title"
                        maxLength={255} 
                    />
                    <AppFormField name="price"
                        placeholder="Price" 
                        maxLength={8}
                        keyboardType="numeric"
                    />
                    <AppFormPicker name="category"
                        numberOfColumns={3}
                        placeholder="Category"
                        items={categories}
                        PickerItemComponent={CategoryPickerItem}
                    />       
                    <AppFormField name="description"
                        placeholder="Description" 
                        maxLength={255} 
                        multiline
                        numberOfLines={3}
                    />
                    <SubmitButton title="Post"/>  
            </AppForm>
        </Screen>
    );
}

const styles = StyleSheet.create({
    container: {
        padding: 10
    }
})

export default ListingEditScreen;