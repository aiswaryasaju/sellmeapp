//import { StatusBar } from 'expo-status-bar';
import React from 'react';
import WelcomeScreen from './app/screens/WelcomeScreen';
import LoginScreen from './app/screens/LoginScreen';
import ListingDetailScreen from './app/screens/ListingDetailScreen';
import ViewImageScreen from './app/screens/ViewImageScreen';
import MessageScreen from './app/screens/MessageScreen';
import AccountScreen from './app/screens/AccountScreen';
import Screen from './app/components/Screen'
import AppPicker from './app/components/AppPicker';
import AppTextInput from './app/components/AppTextInput';
import { TextInput } from 'react-native-gesture-handler';
import AppButton from './app/components/AppButton';
import { Button, Text } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer, useNavigation } from '@react-navigation/native';

const Links = () => {
  const navigation = useNavigation();
  return(
      <Button title="Click"
      onPress={ () => navigation.navigate("TweetsDetails", {id: 1} ) } />
  );
};

const Tweets = ({ navigation }) => (
  <Screen> 
      <Text>Tweets</Text>
      <Links/>
  </Screen>
);

const TweetsDetails = ({ route }) => ( 
  <Screen> 
      <Text>Tweets Details {route.params.id} </Text>
  </Screen>
);

const Stack= createStackNavigator();
const StackNavigator = () => (
    <Stack.Navigator>
        <Stack.Screen name="Tweets" component={Tweets} ></Stack.Screen>
        <Stack.Screen name="TweetsDetails" component={TweetsDetails} ></Stack.Screen>
    </Stack.Navigator>
)

export default function App() {
  return (
    <NavigationContainer>
        <StackNavigator/>
    </NavigationContainer>
    
  );
}


