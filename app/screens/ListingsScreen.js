import React, { useEffect, useState } from 'react';
import { FlatList, StyleSheet } from 'react-native';

import routes from '../navigation/routes';
import Screen from '../components/Screen';
import AppCard from '../components/AppCard';
import colors from '../config/colors';
import listingsApi from '../api/listings';
import AppText from '../components/AppText';
import AppButton from '../components/AppButton';
import ActivityIndicator from '../components/ActivityIndicator';
import useApi from '../hooks/useApi';

function ListingsScreen({navigation}) {

    //get data, error, loading, request from useApi file in hooks folder
    // this is the one way of using the values get from useApi
    //const {data: listings ,error,loading,request: loadListings}= useApi(listingsApi.getListings);

    const getListingsApi= useApi(listingsApi.getListings);

    useEffect( ()=> {
        getListingsApi.request();
    }, [] )

    

    return (
        <>
            <ActivityIndicator visible={getListingsApi.loading} />
            <Screen style={styles.screen}>
                {getListingsApi.error && <> 
                    <AppText>Couldn't retrieve the listings</AppText>
                    <AppButton title="Retry" onPress={getListingsApi.request}></AppButton>
                </>}
                <FlatList 
                    data={getListingsApi.data} 
                    keyExtractor={key => key.id.toString()}
                    renderItem={({item}) =>
                        <AppCard title={item.title}
                        subTitle={"$"+item.price} 
                        imageUrl={item.images[0].url} 
                        thumbnailUrl={item.images[0].thumbnailUrl}
                        onPress={() => navigation.navigate(routes.LISTINGS_DETAILS,item) }
                        />
                    }
                />
            </Screen>
        </>
    );
}

const styles = StyleSheet.create({
    screen : {
        backgroundColor: colors.lightgrey,
        padding: 10
    }
})

export default ListingsScreen;