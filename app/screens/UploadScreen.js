import React from 'react';
import { View, StyleSheet, Modal } from 'react-native';
import * as Progress from 'react-native-progress';
import LotteView from 'lottie-react-native';

import colors from '../config/colors';

function UploadScreen({onDone, progress = 0, visible = false}) {
    return (
        <Modal visible={visible}>
            <View style={styles.container}>
                { progress < 1 ? (
                    <Progress.Bar color={colors.primary} progress={progress} width={200}/>
                    ) : (
                        <LotteView 
                            autoPlay
                            loop= {false}
                            onAnimationFinish={onDone}
                            source={require('../assets/animations/done.json')}
                            style={styles.lotteanimation} />
                    )    
                }    
            </View>
        </Modal>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    lotteanimation: {
        width: 150
    }
});

export default UploadScreen;  