import React from 'react';
import { KeyboardAvoidingView, Platform, View, StyleSheet } from 'react-native';
import {Image} from 'react-native-expo-image-cache';
 
import AppText from '../components/AppText';
import ListItem from '../components/lists/ListItem';
import ContactSellerForm from '../components/ContactSellerForm';
import colors from '../config/colors';

function ListingDetailScreen({route}) {
    const listings= route.params;
    return (
        <KeyboardAvoidingView
            behavior="position"
            keyboardVerticalOffset={Platform.OS === "ios" ? 0 : 100}>
            {/* <Image style={styles.image} source={listings.image[0]}></Image> */}    
            <Image style={styles.image} 
                    tint="light" 
                    preview={{uri: listings.images[0].thumbnailUrl}} 
                    uri={listings.images[0].url}>
            </Image>
            <View style={styles.detailsContainer}>
                <AppText style={styles.title}>{listings.title} </AppText>
                <AppText style={styles.price}>${listings.price}</AppText>
                <View style={styles.userContainer}>
                    <ListItem image={require('../assets/mosh.jpg')}
                        title="Peter John"
                        subTitle="5 Listings">
                    </ListItem>
                </View>
                <ContactSellerForm listing={listings} />
            </View>
        </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    detailsContainer: {
        padding: 15
    },
    image: {
        width: '100%',
        height: 300
    },
    price: {
        color: colors.secondary,
        fontWeight:'bold',
        fontSize: 18
    },
    title: {
        fontWeight:'500',
        fontSize: 18
    },
    userContainer: {
        marginVertical: 40
    },
})

export default ListingDetailScreen;