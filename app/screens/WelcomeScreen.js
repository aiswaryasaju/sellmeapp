import React from 'react';
import { Image, ImageBackground, StyleSheet, Text, View  } from 'react-native';
import AppButton from '../components/AppButton';

import colors from '../config/colors';

function WelcomeScreen({navigation}) {
    return (
        <ImageBackground style={styles.background}
            blurRadius={2}
            source={require("../assets/background.jpg")}>
            <View style={styles.logoContainer}>
                <Image style={styles.logo} source={require("../assets/logo-red.png")}/>    
                <Text style={styles.logoTitle}>Sell What You Don't Need</Text>
            </View>    
            <View style={styles.buttonContainer}>
                <AppButton 
                    title="Login"
                    onPress={()=> navigation.navigate("Login") } />
                <AppButton 
                    title="Register" 
                    color="secondary"
                    onPress={()=> navigation.navigate("Register") } />
            </View>
        </ImageBackground>
    );
}

const styles = StyleSheet.create({
    background: {
        flex: 1,
        justifyContent: "center",
        alignItems: 'center',
    },
    buttonContainer: {
        padding: 10,
        width: '100%',
        height:'100%',
        justifyContent:"flex-end"
    },
    logo: {
        width: 100,
        height: 100,
    },
    logoTitle: {
        fontWeight:"bold",
        fontSize: 20,
        paddingVertical: 5
    },
    logoContainer:{
        position: "absolute",
        alignItems:"center",
        justifyContent:"center",
        paddingBottom: 140
    },
})

export default WelcomeScreen;