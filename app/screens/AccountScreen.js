import React from 'react';
import { useContext } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';

import ListItem from '../components/lists/ListItem';
import ListItemSeperator from '../components/lists/ListItemSeperator';
import Screen from '../components/Screen';
import AppIcon from '../components/AppIcon';
import colors from '../config/colors';
import AuthContext from '../auth/context';
import authStorage from '../auth/storage';
import useAuth from '../auth/useAuth';

const menuItems= [
    {
        title: "My Listings",
        icon: {
            name: "format-list-bulleted",
            backgroundColor: colors.primary
        }
    },
    {
        title: "My Messages",
        icon: {
            name: "email",
            backgroundColor: colors.secondary
        },
        targetScreen: "Messages"
    },
]

function AccountScreen({navigation}) {

    //const {user,setUser} = useContext(AuthContext);
    const {user,logOut} = useAuth(); //custom hook instead of above line
    
    /* handleLogout = () => {
        setUser(null);
        authStorage.removeToken();
    } */

    return (
        <Screen style={styles.screen}>
            <View style={styles.container}>
                <ListItem title={user.name}
                    subTitle={user.email}
                    image={require('../assets/mosh.jpg')}>

                </ListItem>
            </View>
            <View style={styles.container}>
                <FlatList 
                    data={menuItems}
                    keyExtractor={key => key.title} 
                    renderItem={({item}) => 
                        <ListItem 
                            title={item.title}
                            IconComponent={
                                <AppIcon name={item.icon.name} backgroundColor={item.icon.backgroundColor}/>
                            }
                            onPress={() => navigation.navigate(item.targetScreen)} 
                        /> 
                    }
                    ItemSeparatorComponent={ListItemSeperator}
                />
            </View>
            <ListItem 
                title="Log Out"
                IconComponent={
                    <AppIcon name="logout" backgroundColor={colors.yellow}/>
                }
                onPress={() => logOut() } 
            /> 
        </Screen>
    );
}

const styles = StyleSheet.create({
    container: {
        marginVertical: 10
    },
    screen :{
        backgroundColor: colors.lightgrey
    }
})

export default AccountScreen;