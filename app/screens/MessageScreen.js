import React, {useState} from 'react';
import { FlatList, StyleSheet } from 'react-native';

import ListItem from '../components/lists/ListItem';
import ListItemDeleteAction from '../components/lists/ListItemDeleteAction';
import ListItemSeperator from '../components/lists/ListItemSeperator';
import Screen from '../components/Screen';

const initialMessages = [
    {
        id: 1,
        title: "Chair",
        description: "Still Available?",
        image: require('../assets/mosh.jpg')
    },
    {
        id: 2,
        title: "Chair",
        description: "How much for this?",
        image: require('../assets/mosh.jpg')
    },
    {
        id: 3,
        title: "Jacket",
        description: "Is this item available?",
        image: require('../assets/mosh.jpg')
    },
]

function MessageScreen(props) { 
    //here message is the state variable. Initial value of state set to initialMessage(list data)
    //setMessage is a function
    //state is used to redender after deletion
    const [messages,setMessages] = useState(initialMessages);
    const [refreshing,setRefreshing] = useState(false); 

    const handleDelete = (msg) => {
        //delete the msg from messages
        const newMessages= messages.filter(m => m.id !== msg.id);
        setMessages(newMessages);
    }

    return (
       <Screen>
           <FlatList data={messages}
                keyExtractor={key => key.id.toString()}
                renderItem={({item}) =>
                    <ListItem title={item.title}
                        subTitle={item.description}
                        image={item.image}
                        onPress={() => console.log("Selected item is: ",item)}
                        renderRightActions={() => <ListItemDeleteAction onPress={() => handleDelete(item)}/> }
                        />         
                }
                ItemSeparatorComponent={ListItemSeperator}
                refreshing={refreshing}
                onRefresh={ () => {
                    setMessages([
                        {
                            id: 3,
                            title: "Jacket",
                            description: "Is this item available?",
                            image: require('../assets/mosh.jpg')
                        },
                    ])
                }}>
            </FlatList> 
       </Screen>
    );
}

const styles = StyleSheet.create({
   container: {
       padding: 10
   }
})

export default MessageScreen;