import React, { useState } from 'react';
import { StyleSheet, Image } from 'react-native';
import * as Yup from 'yup' ;

import Screen from '../components/Screen';
import { 
    ErrorMessage, 
    AppForm, 
    AppFormField, 
    SubmitButton 
} from '../components/forms'; //As we add index.js flle in the form folder, we can combine the three import statements into one
import authApi from '../api/auth';
import useAuth from '../auth/useAuth';

const validateSchema = Yup.object().shape({
    email: Yup.string().required().email().label("Email"),
    password: Yup.string().required().min(4).label("Password")
});

function LoginScreen(props) {

    //const authContext = useContext(AuthContext);
    const auth = useAuth();
    const [loginFailed, setLoginFailed] = useState(false);

    const handleSubmit= async ({email,password}) => {
        const result =await authApi.login(email,password);
        if(!result.ok) return setLoginFailed(true);
        setLoginFailed(false);
        console.log("Login Success: ",result.data);

        //Decode the user from auth token
        auth.logIn(result.data);
    }

    return (
        <Screen style={styles.container}>
            <Image style={styles.logo} source={require("../assets/logo-red.png")}/>  
            <AppForm
                initialValues={{email: "",password: ""}}
                onSubmit={handleSubmit}
                validationSchema= {validateSchema}>
                <ErrorMessage error="Invalid email and/or password." visible={loginFailed} />    
                <AppFormField name="email"
                    icon="email" 
                    placeholder="Email" 
                    autoCapitalize="none" 
                    autoCorrect={false} 
                    keyboardType="email-address"
                    textContentType="emailAddress"
                />
                <AppFormField name="password"
                    icon="lock" 
                    placeholder="Password" 
                    autoCapitalize="none" 
                    autoCorrect={false} 
                    secureTextEntry={true}
                    password={true}
                    textContentType="password"
                    //onChangeText={handleChange("password")}
                    //onBlur={() => setFieldTouched("password")}
                />
                <SubmitButton title="Login"/>    
            </AppForm>     
        </Screen>
    );
}

const styles = StyleSheet.create({
    container: {
        padding: 10,
    },
    logo: {
        width: 100,
        height: 100,
        alignSelf:'center',
        marginTop: 80,
        marginBottom: 20
    }
})

export default LoginScreen;