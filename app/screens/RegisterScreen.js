import React, {useState} from 'react';
import { StyleSheet } from 'react-native';
import * as Yup from 'yup';

import { 
    AppForm, 
    AppFormField, 
    ErrorMessage, 
    SubmitButton 
} from '../components/forms';
import Screen from '../components/Screen';
import usersApi from '../api/users';
import authApi from '../api/auth';
import useAuth from "../auth/useAuth";
import useApi from '../hooks/useApi';
import ActivityIndicator from '../components/ActivityIndicator';


const validateSchema = Yup.object().shape({
    name: Yup.string().required().label("Name"),
    email: Yup.string().required().email().label("Email"),
    password: Yup.string().required().min(4).label("Password")
});

function RegisterScreen(props) {

    const registerApi = useApi(usersApi.register); //for showing loading indicator 
    const loginApi = useApi(authApi.login);
    const auth = useAuth();
    const [error, setError] = useState();

    const handleSubmit= async (userInfo) => {
        const result = await registerApi.request(userInfo);

        if(!result.ok) {
            if(result.data) setError(result.data.error);
            else {
                setError("An unexpected error occured.");
                console.log(result);
            }
            return;
        }

        //if successfull register, login the user using authapi
        //{data: authToken} destructuring the result and rename the data property to authToken
        const {data: authToken} = await loginApi.request(userInfo.email,userInfo.password);
        auth.logIn(authToken);
    }
    return (
        <>
            <ActivityIndicator visible={registerApi.loading || loginApi.loading}/>
            <Screen style={styles.container}>
                <AppForm initialValues={{name: "",password: "",email: ""}}
                    onSubmit={handleSubmit}
                    validationSchema={validateSchema}>
                    <ErrorMessage error={error} visible={error} />    
                    <AppFormField name="name"
                        icon="account" 
                        placeholder="Name" 
                        autoCapitalize="none" 
                        autoCorrect={false} 
                        keyboardType="default"
                        textContentType="name"
                    />    
                    <AppFormField name="email"
                        icon="email" 
                        placeholder="Email" 
                        autoCapitalize="none" 
                        autoCorrect={false} 
                        keyboardType="email-address"
                        textContentType="emailAddress"
                    />
                    <AppFormField name="password"
                        icon="lock" 
                        placeholder="Password" 
                        autoCapitalize="none" 
                        autoCorrect={false} 
                        secureTextEntry={true}
                        password={true}
                        textContentType="password"
                    />
                    <SubmitButton title="Register"/>  
                </AppForm>
            </Screen>
        </>
    );
}
const styles = StyleSheet.create({
    container: {
        padding: 10
    }
})

export default RegisterScreen;