import React, { useEffect } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import * as Notifications from 'expo-notifications';
import * as Permissions from 'expo-permissions';
import { Platform } from 'react-native';

import ListingEditScreen from '../screens/ListingEditScreen';
import FeedNavigator from './FeedNavigator';
import AccountNavigator from './AccountNavigator';
import NewListingButton from './NewListingButton';
import expoPushTokensApi from '../api/expoPushTokens';

const Tab= createBottomTabNavigator();
const AppNavigator= () => {

    useEffect( () => {
        registerPushNotification();
        
    },[]);

    const registerPushNotification = async () => {
        try {
            const permission = await Permissions.askAsync(Permissions.NOTIFICATIONS);
            if(!permission.granted) return;
            const token = await Notifications.getExpoPushTokenAsync();
            expoPushTokensApi.register(token.data);
        } catch (error) {
            console.log(error);
        }
    }
    return (
        <Tab.Navigator>
            <Tab.Screen 
                name="Feed"
                component={FeedNavigator} 
                options={{
                    tabBarIcon: ({size,color}) => 
                    <MaterialCommunityIcons name="home" size={size} color={color}/> 
                }}/>
            <Tab.Screen
                name="ListingEdit"
                component={ListingEditScreen}
                options={({navigation}) =>  ({
                    tabBarButton: () => <NewListingButton onPress={ () => navigation.navigate("ListingEdit") } />,
                    tabBarIcon: ({size,color}) => 
                    <MaterialCommunityIcons name="plus-circle" size={size} color={color}/> 
                })}/> 
            <Tab.Screen
                name="Account"
                component={AccountNavigator}
                options={{
                    tabBarIcon: ({size,color}) => 
                    <MaterialCommunityIcons name="account" size={size} color={color}/> 
                }}/>
        </Tab.Navigator>
    )
} 

export default AppNavigator;