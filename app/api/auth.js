import { exp } from 'react-native/Libraries/Animated/src/Easing';
import client from './client';

const login = (email,password) => client.post('/auth', {email,password});

export default{
    login,
};