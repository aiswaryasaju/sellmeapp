import client from './client';

//const register = (name,email,password) => client.post('/auth', {name,email,password});
//Instead of adding three parameters, we are passing these values as single object(userInfo) that encapsulate all these values. In future if we want to add new field to register page, no need to modify this page. 
const register = (userInfo) => client.post('/users',userInfo);

export default{
    register,
};