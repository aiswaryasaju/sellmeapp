import React, {useEffect} from 'react';
import { View,Image, StyleSheet, TouchableWithoutFeedback, Alert } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import * as ImagePicker from 'expo-image-picker';

import colors from '../config/colors';

function AppImageInput({imageUri,onChangeImage}) {

    useEffect( () => {
        requestPermission();
    },[])

    const requestPermission = async () => {
        const result= await ImagePicker.requestCameraRollPermissionsAsync();
        if(!result.granted){
            alert('You need to enable permission to access the library');
        }
    }
    
    const handlePress = () => {
        if(!imageUri) selectImage();
        else Alert.alert('Delete', 'Are you sure you want to delete the image?',
        [ {text: 'Yes', onPress: () => onChangeImage(null) },
          {text: 'No',}
        ]);
    }

    const selectImage = async () => {
        try {
            const result= await ImagePicker.launchImageLibraryAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.Images,
                quality: 0.5
            });
            if(!result.cancelled){
                onChangeImage(result.uri);
            }
        } catch (error) {
            console.log('Error reading an Image', error);
        }
    }
    
    return (
        <TouchableWithoutFeedback onPress={handlePress}>
            <View style={styles.container}>
                {!imageUri && <MaterialCommunityIcons name="camera" color={colors.mediumgrey} size={40}/>}
                {imageUri && <Image style={styles.image} source={{uri: imageUri}}></Image>}
            </View>
        </TouchableWithoutFeedback>
        
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.lightgrey,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
        height: 100,
        width: 100,
        overflow: 'hidden'
    },
  image: {
      width: '100%',
      height: '100%'
  }
});

export default AppImageInput;