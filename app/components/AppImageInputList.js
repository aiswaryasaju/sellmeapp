import React, { useRef } from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import AppImageInput from './AppImageInput';

function AppImageInputList({imageUris = [], onRemoveImage, onAddImage}) {
  const scrollView = useRef();
  return (
      <View> 
          <ScrollView 
              ref={scrollView} 
              horizontal={true}
              onContentSizeChange={() => scrollView.current.scrollToEnd()}>
              <View style={styles.container}>
                {imageUris.map(uri => (
                  //using key because map.
                    <View key={uri} style={styles.image}> 
                        <AppImageInput 
                            imageUri={uri}
                            onChangeImage={()=>onRemoveImage(uri)}
                        />
                    </View>     
                ))}
                <AppImageInput onChangeImage={(uri) => onAddImage(uri) } />
              </View>
          </ScrollView>
      </View> //if this view is not there, the scrollview will fill the whole screen
  );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row'
    },
    image: {
        marginRight: 10
    }
});

export default AppImageInputList;