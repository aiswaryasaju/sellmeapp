import React from 'react';
import { useFormikContext } from 'formik';

import ErrorMessage from './ErrorMessage';
import AppPicker from '../AppPicker';

function AppFormPicker({PickerItemComponent, numberOfColumns, items, name, placeholder}) {
    const { setFieldValue, errors, touched, values } = useFormikContext();
    return (
        <>
            <AppPicker items={items}
                numberOfColumns={numberOfColumns}
                PickerItemComponent={PickerItemComponent}
                onSelectItem={(item) => setFieldValue(name,item)} 
                placeholder={placeholder} 
                selectedItem={values[name]}>
            </AppPicker>
            <ErrorMessage error={errors[name]} visible={touched[name]}></ErrorMessage>    
        </>
    );
}

export default AppFormPicker;