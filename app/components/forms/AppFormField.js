import React from 'react';
import { useFormikContext } from 'formik';

import AppTextInput from '../AppTextInput';
import ErrorMessage from './ErrorMessage';

function AppFormField({name, ...otherProps }) {
    const { 
        setFieldTouched,
        setFieldValue, 
        errors, 
        touched, 
        values
    } = useFormikContext();
    return (
        <>
            <AppTextInput 
                onChangeText={(text) => setFieldValue(name,text)}
                onBlur={() => setFieldTouched(name)}
                value={values[name]}
                {...otherProps}
                // autoCapitalize="none" 
                // autoCorrect={false} 
                // icon="email" 
                // keyboardType="email-address" 
                // placeholder="Email"                        
                // textContentType="emailAddress"
            />
            <ErrorMessage error={errors[name]} visible={touched[name]}></ErrorMessage>    
        </>
    );
}

export default AppFormField;

//this is not formik component to get setFieldTouched, handleChange, errors, touched. 
//Inorder to get these values we need to use formik context