import React from 'react';
import { View , StyleSheet} from 'react-native';
import colors from '../../config/colors';
import AppText from '../AppText';

function ErrorMessage({error,visible}) {    
        if(!visible || !error) return null;
        return (
            <AppText style={styles.errorText}>{error}</AppText>
        );
}

const styles = StyleSheet.create({
    errorText: {
        fontSize: 14,
        color: 'red'
    }
})
export default ErrorMessage;