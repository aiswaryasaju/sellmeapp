import React from 'react';
import { useFormikContext } from 'formik';
import AppButton from '../AppButton';

function SubmitButton({title}) {
    const{handleSubmit} = useFormikContext(); //It returns the handleSubmit as object
    return (
        <AppButton title={title} onPress={handleSubmit}></AppButton>  
    );
}

export default SubmitButton;

//this is not formik component to get handleSubmit. 
//Inorder to get handleSubmit we need to use formik context.