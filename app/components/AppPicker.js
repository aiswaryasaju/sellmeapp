import React, { useState } from 'react';
import { TextInput, View, StyleSheet, TouchableWithoutFeedback, Modal, Button, FlatList } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';

import defaultStyles from '../config/styles';
import AppText from './AppText';
import Screen from './Screen';
import PickerItems from './PickerItems';
import colors from '../config/colors';

function AppPicker({PickerItemComponent= PickerItems, numberOfColumns=1, icon, items, onSelectItem, placeholder, selectedItem}) {
    const [modelVisible, setModelVisible] = useState(false);
    return (
        <> 
            <TouchableWithoutFeedback onPress={() => setModelVisible(true)}>
                <View style={styles.container}> 
                    {icon && <MaterialCommunityIcons name={icon} size={20} color={defaultStyles.colors.darkgrey} style={styles.icon} />}
                    { selectedItem ? 
                        (<AppText style={styles.text}>{selectedItem.label}</AppText>) : 
                        (<AppText style={styles.placeholder}>{placeholder}</AppText>)
                    }
                    <MaterialCommunityIcons name="chevron-down" size={20} color={defaultStyles.colors.darkgrey}/>
                </View>
            </TouchableWithoutFeedback>
            <Modal visible={modelVisible} animationType='slide'>
                <Screen>
                    <Button title="Close" onPress={() => setModelVisible(false)} />
                    <FlatList data={items}
                        numColumns={numberOfColumns}
                        keyExtractor={key => key.value.toString()}
                        renderItem= {({item}) => 
                            <PickerItemComponent 
                                item={item} //set it to item that we going to render
                                label= {item.label}
                                onPress={() => {
                                        setModelVisible(false);
                                        onSelectItem(item);
                                    } 
                                }
                            /> 
                        }
                    />
                </Screen>
            </Modal>
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: defaultStyles.colors.lightgrey,
        borderRadius: 25,
        flexDirection: 'row',
        width: '100%',
        padding: 15,
        marginVertical: 10
    },
    icon: {
        marginRight: 10
    },
    placeholder: {
        color: defaultStyles.colors.mediumgrey,
        flex: 1
    },
    text: {
        flex: 1,
        
    }
})

export default AppPicker;