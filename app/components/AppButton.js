import React from 'react';
import { StyleSheet, TouchableHighlight, Text} from 'react-native';

import colors from '../config/colors';

function AppButton({title,onPress,color="primary"}) {
    return (
        <TouchableHighlight style={[styles.button,{backgroundColor: colors[color]}]} onPress={onPress}>
            <Text style={styles.text}>{title}</Text>
        </TouchableHighlight>
    );
}

const styles = StyleSheet.create({
    button: {
        width:'100%',
        padding: 12,
        marginTop: 15,
        marginVertical: 5,
        overlayColor: colors.black,
        backgroundColor: colors.primary,
        borderRadius: 25,
        justifyContent: 'center',
        alignItems: 'center'
    }, 
    text: {
        color: colors.white,
        fontSize: 18,
        textTransform: 'uppercase',
        fontWeight: 'bold'
    }
})

export default AppButton;