import React from 'react';
import { Image, View, StyleSheet, TouchableHighlight } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import Swipeable from 'react-native-gesture-handler/Swipeable';

import colors from '../../config/colors';
import AppText from '../AppText';

function ListItem({image,IconComponent,title,subTitle,onPress,renderRightActions}) {
    return (
        <Swipeable renderRightActions={renderRightActions}>
            <TouchableHighlight underlayColor={colors.lightgrey} 
                onPress={onPress}>
                <View style={styles.container}>
                    {IconComponent}
                    {image && <Image style={styles.image} source={image}></Image>} 
                    <View style={styles.titleContainer}>
                        <AppText style={styles.title} numberOfLines={1}>{title}</AppText>
                        {subTitle && <AppText style={styles.subTitle} numberOfLines={1}>{subTitle}</AppText>}
                    </View>
                    <MaterialCommunityIcons name="chevron-right" color={colors.mediumgrey} size={25} />
                </View>
            </TouchableHighlight>
        </Swipeable>
    );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        backgroundColor: colors.white,
        alignItems: 'center',
        padding: 15
    },
    image: {
        width: 60,
        height: 60,
        borderRadius: 30,
    },
    subTitle:{
        color: colors.mediumgrey
    },
    title:{
        fontSize: 16,
        fontWeight: '600'
    },
    titleContainer: {
        flex: 1,
        marginLeft: 10,
        justifyContent:'center'
    },
    
})

export default ListItem;