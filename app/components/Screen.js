import React from 'react';
import { Platform, SafeAreaView, StatusBar, StyleSheet, View } from 'react-native';

import Constants from 'expo-constants';

function Screen({children,style}) {
    return (
        <SafeAreaView style={[styles.container, style]}>
            <View style={[styles.view, style]}>
                {children}
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        //paddingTop: Constants.statusBarHeight
        paddingTop: Platform.OS==="android" ? StatusBar.currentHeight : 0,
        flex: 1
    },
    view: {
        padding: 10,
        flex: 1
    }
})

export default Screen;