import React from 'react';
import { View, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import {Image} from 'react-native-expo-image-cache';

import AppText from './AppText';
import colors from '../config/colors';

function AppCard({title,subTitle,imageUrl,onPress,thumbnailUrl}) {
    return (
        <TouchableWithoutFeedback onPress={onPress}>
            <View style={styles.card}>
                {/* <Image style={styles.image} source={{uri: imageUrl}}></Image> */}
                <Image style={styles.image}
                    tint="light" 
                    preview={{uri: thumbnailUrl}}
                    uri={imageUrl}>
                </Image>
                <View style={styles.detailsContainer}>
                    <AppText style={styles.title} numberOfLines={1}>{title}</AppText>
                    <AppText style={styles.subTitle} numberOfLines={2}>{subTitle}</AppText>
                </View>
            </View>
        </TouchableWithoutFeedback>
       
    );
}

const styles = StyleSheet.create({
    card: {
        borderRadius: 5,
        backgroundColor: colors.white,
        marginBottom: 20,
        overflow: 'hidden'
    },
    detailsContainer:{
        padding: 12
    },
    image: {
        width: '100%',
        height: 200,
    },
    subTitle: {
        color: colors.secondary,
        fontWeight: 'bold'
    },
    title: {
        marginBottom: 5
    },
})

export default AppCard;