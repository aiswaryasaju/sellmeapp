import * as SecureStore from 'expo-secure-store';
import jwtDecode from 'jwt-decode';

const key = "authToken";

const storeToken = async authToken => {
    try {
        await SecureStore.setItemAsync(key,authToken);
    } catch (error) {
        console.log("Error storing Auth Token: ",error);
    }
}

const getUser = async () => {
    try {
        const token = await getToken();
        return (token) ? jwtDecode(token) : null;
        // if(token) return jwtDecode(token);
        // return null;    
    } catch (error) {
        console.log("Error getting Auth Token: ",error);
    }
}

const getToken = async () => {
    try {
        const authToken = await SecureStore.getItemAsync(key);
        return authToken;
    } catch (error) {
        console.log("Error getting Auth Token: ",error);
    }
}

const removeToken = async () => {
    try {
        await SecureStore.deleteItemAsync(key);
    } catch (error) {
        console.log("Error deleting Auth Token: ",error);
    }
}

export default {getToken, storeToken, removeToken, getUser};