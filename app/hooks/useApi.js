import {useState} from 'react';

export default useApi = (apiFunc) => {
    const [data,setData] = useState([]);
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(false);

    const request = async(...args) => {

        //for showing loading indicator
        setLoading(true);
        const response = await apiFunc(...args);
        setLoading(false);

        setError(!response.ok);
        setData(response.data);
        return response;

        /*
        //if ok returns false we have an error
        if(!response.ok){
            setError(true);
            return response;
           // response.problem gives type of error
        }
        //Load data
        setError(false);
        setData(response.data);
        return response;
        */
    }
    return {data, error, loading, request};
}