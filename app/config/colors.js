export default{
    primary: "#fc5c65",
    secondary:"#4ecdc4",
    black: "#000",
    white: "#fff",
    darkgrey: "#0c0c0c",
    lightgrey: "#f8f4f4",
    mediumgrey: "#6e6969",
    red: "#ff5252",
    yellow: "#ffe66d",

    background: "#f67280",
    button1:"#7B1FA2",
    button2: "#f67280",
    text: "#fff"
}